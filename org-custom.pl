#!/usr/bin/env perl
# ====================[ orgaddition.pl                  ]====================

=head1 NAME

org-custom - An Oddmuse module for augmenting the Org Markup module with
                 so-called, unofficial "Org Additions" markup.

=head1 INSTALLATION

orgaddition is simply installable; simply:

=over

=item First install the Org Markup module; see
      L<http://www.oddmuse.org/cgi-bin/oddmuse/Org_Markup_Extension>.

=item Move this file into the B<wiki/modules/> directory for your Oddmuse Wiki.

=back

=cut
# # package OddMuse;

$ModulesDescription .= '<p>See <a href="http://oddmuse.org/wiki/Org_Markup_Extension">Org Markup Extension</a></p>';

*GetOldTitle = *GetTitle;
*GetTitle = *OrgGetTitle;

sub OrgGetTitle {
  my ($id, $title, $oldId, $nocache, $status, $links) = @_;
  return '';
}

*GetOldHeader = *GetHeader;
*GetHeader = *OrgGetHeader;

sub OrgGetHeader {
  my ($id, $title, $oldId, $nocache, $status, $links) = @_;
  return GetOldHeader($id, $title, $oldId, $nocache, $status, $links)
    . GetOldTitle($id, $title);
}

push(@MyFooters, \&OrgPrintFooter);
sub OrgPrintFooter {
  my ($id) = @_;
  # return $q->div({-class=>'navbar'}, GetGotoBar(@_));
  return '';
}

use vars qw($SideBars);

# Include this page on every page:
@SideBars = ();

# do this later so that the user can customize $SidebarName
push(@MyInitVariables, \&SideBarInit);

sub SideBarInit {
  foreach my $sideBar (@SideBars) {
    $AdminPages{FreeToNormal($sideBar)} = 1;
  }
}

sub PrintSideBar {
  my ($pageName, $className) = @_;

  $pageName = FreeToNormal($pageName);
  $className = lc($pageName) unless $className;
  # While rendering, OpenPageName must point to the sidebar, so that
  # the form extension which checks whether the current page is locked
  # will check the SideBar lock and not the real page's lock.
  local $OpenPageName = $pageName;
  print $q->start_div({-class=>$className});
  # This makes sure that $Page{text} remains undisturbed.
  PrintWikiToHTML(GetPageContent($pageName));
  print $q->end_div();
  return '';
}

sub PrintMyContent {
  foreach my $sideBar (@SideBars) {
    PrintSideBar($sideBar);
  }
}

=head1 COPYRIGHT AND LICENSE

The information below applies to everything in this distribution,
except where noted.

Copyright 2013  Jambunathan K <kjambunathan@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see L<http://www.gnu.org/licenses/>.

=cut
