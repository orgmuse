#!/usr/bin/env perl
# ====================[ org.pl                          ]====================

=head1 NAME

org - An Oddmuse module for marking up Oddmuse Wiki pages according to the
         Wiki Org standard, a Wiki-agnostic syntax scheme.

=head1 INSTALLATION

org is easily installable; move this file into the B<wiki/modules/>
directory for your Oddmuse Wiki.

=cut
package OddMuse;

$ModulesDescription .= '<p>See <a href="http://oddmuse.org/wiki/Org_Markup_Extension">Org Markup Extension</a></p>';

use Data::Dumper;		# For debugging

# ....................{ CONFIGURATION                      }....................

=head1 CONFIGURATION

org is easily configurable; set these variables in the
B<wiki/config.pl> file for your Oddmuse Wiki.

=cut
use vars qw($OrgLineBreaks
	    $OrgTableCellsContainBlockLevelElements
	    $FootnoteFormat
	    $FootnoteSeparator
	    $FootnotesPattern
            $FootnotesHeaderText
	    $FootnoteAnonymousCount
	    %FootnoteRecord
            @FootnoteLabels);

=head2 $OrgEmphasisRegexpPattern

Regular expression to match emphasized text.  On successful match, the
specific marker used for emphasis (i.e, one of C<*> C</> C<_> C<=>
C<+> C<~>) and the text between those markers is assumed to be
available in the variables C<$+{marker}> and C<$+{text}> respectively.

=cut

my $OrgEmphasisRegexpPattern =
  qr! \G
      (?<pre>^|(?<=[\ \t('\"{\n]))
      (
	(?<marker>[=*/_+~])	#  match verbatim marker first
	(?<text>
	  \S |
	  (?<pre_border>[^\ \t\r\n,'\"])
	  (?<body>[^\n]*?(?:\n[^\n]*?){0,3})
	  (?<post_border>[^\ \t\r\n,'\"])
	)
	\k<marker>
      )
      (?<post>(?=[-\ \t.,:?;'\")\n])|$)
    !x;

my $OrgEmphasisStartPattern =
  qr@ \G
      (?<pre>^|(?<=[\ \t('\"{\n]))
      (?<marker>[=*/_+~])
      (?<pre_border>[^\ \t\r\n,'\"])
    @x;

my $OrgEmphasisEndPattern =
  qr@ \G
      (?<post_border>[^\ \t\r\n,'\"])
      (?<marker>[=*/_+~])
      (?<post>(?=[-\ \t.,:?;'\")\n])|$)
    @x;

my %OrgEmphasisAlist =  ('**' => 'strong',
			 '//' => 'em',
			 '__' => 'u',
			 '==' => 'code', # Text between `=' marker is
                                        # NEVER parsed for org syntax.
			 '~~' => 'tt',	# Text between `~' marker is
                                        # rendered in monospace font.
                                        # It is parsed for Org syntax.
			 '++' => 'del',
			 '*' => 'strong',
			 '/' => 'em',
			 '_' => 'u',
			 '=' => 'code', # Text between `=' marker is
                                        # NEVER parsed for org syntax.
			 '~' => 'tt',	# Text between `~' marker is
                                        # rendered in monospace font.
                                        # It is parsed for Org syntax.
			 '+' => 'del',
			);

=head2 $OrgLineBreaks

A boolean that, if true, causes this extension to convert single
newlines in page text to genuine linebreaks (i.e., the <br> tag) in
the HTML for that page. (If false, this extension consumes single
newlines without actually converting them into anything; they will be
ignored, wherever found.)

Irregardless of this booleans setting, this extension always converts
two newlines to a paragraph break (i.e., the <p> tag).

=cut
$OrgLineBreaks = 0;

=head2 $OrgTableCellsContainBlockLevelElements

A boolean that, if true, permits table cell markup to embed block
level elements in table cells. (By default, this boolean is false.)

You are encouraged to enable this boolean, as it significantly
improves the "stuff" you can do with Wiki Org table syntax. For
example, enabling this boolean permits you to embed nested lists in
tables.

Block level elements are such "high-level" entities as paragraphs,
blockquotes, list items, and so on. Thus, enabling this boolean
permits you to embed multiple paragraphs, blockquotes, and so on in
individual table cells.

Please note: enabling this boolean permits non-conformant syntax --
that is, syntax which no longer conforms to the Wiki Org standard. (In
general, unless you have significant amounts of Wiki Org table markup
strictly conforming to the Wiki Org standard, this shouldn't be an
issue.)

Please note: enabling this boolean also requires you explicitly close
the last table cell of a cell with a "|" character. (This character is
optional under the Wiki Org standard, but not under this
non-conformant alteration.)

=cut
$OrgTableCellsContainBlockLevelElements = 0;

=head2 $OrgTableHtmlAttributes

Html attributes that apply to an Org table.  The default setting draws
rules at the top and bottom of the table and between the row and
column groups.

=cut
$OrgTableHtmlAttributes = 'border="2" rules="groups" frame="hsides"';

=head2 $FootnoteFormat

Format specifier for typesetting of footnote references.  Replace
C<%s> specifier in this string with footnote number.

The default value is C<<sup>%s</sup>>.  i.e., Typeset footnote
references as superscripts.

=cut
$FootnoteFormat = '<sup>%s</sup>';

=head2 $FootnoteSeparator

=cut
$FootnoteSeparator = ', ';

=head2 $FootnotesHeaderText

Outline header text for footnote definitions.

=cut
$FootnotesHeaderText = 'Footnotes:';

=head2 $FootnotesPattern

Replace text matching this regular expression with footnote
definitions.  If a page specifies no such markup, emit footnote
definitions between the content and footer for that page.

Default value is C<\&lt;footnotes\&gt;[ \t]*(\n|$)>.

=cut
$FootnotesPattern = '\&lt;footnotes\&gt;[ \t]*(\n|$)';

=head2 MARKUP

Org handles additional markup resembling:

  <footnotes>

See C<$FootnotesPattern>.

=cut

# ....................{ INITIALIZATION                     }....................
push(@MyInitVariables, \&OrgInit);

# A boolean that is true if the "orgaddition.pl" module is also installed.
my $OrgIsOrgAddition;

# A boolean set by OrgTextMarkupRule() to true, if a new table cell
# has just been started. This allows testing, elsewhere, of whether we
# are at the start of a a new table cell. Why test that? Because. If
# we are indeed at the start of a a new table cell, we should behave
# as if the "$bol_fixme" boolean is true: we should allow block level
# elements at the start of this new table cell.
#
# Of course, we have to set this to false immediately after matching
# past the start of that table cell. This is what RunMyRulesOrg()
# does.
my $OrgIsTableCellBol;

# A regular expression matching Wiki Org-style table cells.
my $OrgTableCellPattern = '[ \t]*(\|+)(=)?\n?([ \t]*)';

# A regular rexpression matching a horizontal ruler in an Org table.
# The regex doesn't include leading and trailing newlines.
my $OrgTableHrulePattern = '[ \t]*\|([-]+[+])*([-]+)\|[ \t]*';

# A regular expression matching Wiki Org-style pipe delimiters in
# links.
my $OrgLinkPipePattern = '[ \t]*\]\[[ \t]*';

# A regular expression matching Wiki Org-style link text. This
# expression takes into account the fact that such text is always
# optional.
my $OrgLinkTextPattern = "($OrgLinkPipePattern(.+?))?";

# The html tag and string of html tag attributes for the current Org
# header.  This prevents an otherwise necessary, costly evaluation of
# test statements resembling:
#
#  if (InElement('h1') or InElement('h2') or InElement('h3') or
#      InElement('h4') or InElement('h5') or InElement('h6')) { ... }
#
# As Org headers cannot span blocks or lines, this should be a safe
# caching.
my ($OrgHeaderHtmlTag, $OrgHeaderHtmlTagAttr);

# For list of languages supported by Kate, see
# %NativeLanguageToKateLanguage in syntax-highlight.pl.

my %OrgLanguageToKateLanguage =
  (
   # ".desktop"=>".desktop",
   # "4GL"=>"4GL",
   # "4GL-PER"=>"4GL-PER",
   # "ABC"=>"ABC",
   # "AHDL"=>"AHDL",
   # "ANSI C89"=>"ANSI C89",
   # "ASP"=>"ASP",
   # "AVR Assembler"=>"AVR Assembler",
   "awk"=>"AWK",
   "ada"=>"Ada",
   # "Alerts"=>"Alerts",
   # "Ansys"=>"Ansys",
   # "Apache Configuration"=>"Apache Configuration",
   # "Asm6502"=>"Asm6502",
   "sh"=>"Bash",
   "bibtex"=>"BibTeX",
   "c"=>"C",
   # "C#"=>"C#",
   "c++"=>"C++",
   # "CGiS"=>"CGiS",
   # "CMake"=>"CMake",
   "css"=>"CSS",
   # "CSS/PHP"=>"CSS/PHP",
   # "CUE Sheet"=>"CUE Sheet",
   # "Cg"=>"Cg",
   "change-log"=>"ChangeLog",
   # "Cisco"=>"Cisco",
   # "Clipper"=>"Clipper",
   # "ColdFusion"=>"ColdFusion",
   "lisp"=>"Common Lisp",
   "emacs-lisp"=>"Common Lisp",
   # "Component-Pascal"=>"Component-Pascal",
   # "D"=>"D",
   # "Debian Changelog"=>"Debian Changelog",
   # "Debian Control"=>"Debian Control",
   "diff"=>"Diff",
   # "Doxygen"=>"Doxygen",
   # "E Language"=>"E Language",
   # "Eiffel"=>"Eiffel",
   # "Email"=>"Email",
   # "Euphoria"=>"Euphoria",
   "fortran"=>"Fortran",
   # "FreeBASIC"=>"FreeBASIC",
   # "GDL"=>"GDL",
   # "GLSL"=>"GLSL",
   # "GNU Assembler"=>"GNU Assembler",
   # "GNU Gettext"=>"GNU Gettext",
   "html"=>"HTML",
   # "Haskell"=>"Haskell",
   # "IDL"=>"IDL",
   # "ILERPG"=>"ILERPG",
   # "INI Files"=>"INI Files",
   # "Inform"=>"Inform",
   # "Intel x86 (NASM)"=>"Intel x86 (NASM)",
   # "JSP"=>"JSP",
   "java"=>"Java",
   "javascript"=>"JavaScript",
   "JavaScript/PHP"=>"JavaScript/PHP",
   # "Javadoc"=>"Javadoc",
   # "KBasic"=>"KBasic",
   # "Kate File Template"=>"Kate File Template",
   # "LDIF"=>"LDIF",
   # "LPC"=>"LPC",
   "latex"=>"LaTeX",
   # "Lex/Flex"=>"Lex/Flex",
   # "LilyPond"=>"LilyPond",
   # "Literate Haskell"=>"Literate Haskell",
   # "Logtalk"=>"Logtalk",
   # "Lua"=>"Lua",
   # "M3U"=>"M3U",
   # "MAB-DB"=>"MAB-DB",
   # "MIPS Assembler"=>"MIPS Assembler",
   "makefile"=>"Makefile",
   # "Mason"=>"Mason",
   # "Matlab"=>"Matlab",
   # "Modula-2"=>"Modula-2",
   # "Music Publisher"=>"Music Publisher",
   # "Objective Caml"=>"Objective Caml",
   # "Objective-C"=>"Objective-C",
   "octave"=>"Octave",
   # "PHP (HTML)"=>"PHP (HTML)",
   # "PHP/PHP"=>"PHP/PHP",
   # "POV-Ray"=>"POV-Ray",
   "pascal"=>"Pascal",
   "perl"=>"Perl",
   # "PicAsm"=>"PicAsm",
   # "Pike"=>"Pike",
   # "PostScript"=>"PostScript",
   # "Prolog"=>"Prolog",
   # "PureBasic"=>"PureBasic",
   "python"=>"Python",
   # "Quake Script"=>"Quake Script",
   # "R Script"=>"R Script",
   # "REXX"=>"REXX",
   # "RPM Spec"=>"RPM Spec",
   # "RSI IDL"=>"RSI IDL",
   # "RenderMan RIB"=>"RenderMan RIB",
   "ruby"=>"Ruby",
   "sgml"=>"SGML",
   # "SML"=>"SML",
   # "SQL (MySQL)"=>"SQL (MySQL)",
   # "SQL (PostgreSQL)"=>"SQL (PostgreSQL)",
   "sql"=>"SQL",
   # "Sather"=>"Sather",
   "scheme"=>"Scheme",
   # "Sieve"=>"Sieve",
   # "Spice"=>"Spice",
   # "Stata"=>"Stata",
   # "TI Basic"=>"TI Basic",
   # "TaskJuggler"=>"TaskJuggler",
   "tcl"=>"Tcl/Tk",
   # "UnrealScript"=>"UnrealScript",
   # "VHDL"=>"VHDL",
   # "VRML"=>"VRML",
   # "Velocity"=>"Velocity",
   # "verilog"=>"Verilog",
   # "WINE Config"=>"WINE Config",
   # "Wikimedia"=>"Wikimedia",
   "xml"=>"XML",
   "nxml"=>"XML",
   # "Yacc/Bison"=>"Yacc/Bison",
   # "de_DE"=>"de_DE",
   # "en_US"=>"en_US",
   # "ferite"=>"ferite",
   # "nl"=>"nl",
   # "progress"=>"progress",
   # "scilab"=>"scilab",
   # "txt2tags"=>"txt2tags",
   # "x.org Configuration"=>"x.org Configuration",
   # "xHarbour"=>"xHarbour",
   # "xslt"=>"xslt",
   # "yacas"=>"yacas"
  );

sub OrgInit {
  $OrgIsOrgAddition = defined &OrgAdditionRule;

  $OrgIsTableCellBol =
  $OrgHeaderHtmlTag =
  $OrgHeaderHtmlTagAttr = '';

  # This is the "code magic" enabling block-level elements in multi-line
  # table cells.
  if ($OrgTableCellsContainBlockLevelElements) {
    SetHtmlEnvironmentContainer('td');
    SetHtmlEnvironmentContainer('th');
  }

  # Initialize footnote variables.
  @FootnoteLabels = ();
  %FootnoteList = {};
  $FootnoteAnonymousCount = 0;

  # FIXME: The following changes interfere with the bbcode extension.
  # To achieve something similar, we often see sites with an InterMap
  # entry called Self, eg. from http://emacswiki.org/InterMap: Self
  # /cgi-bin/emacs? -- which allows you to link to Self:action=index.

  # Permit page authors to link to URLs resembling:
  #   "See [[/?action=index|the site map]]."
  #
  # Which Oddmuse converts to HTML resembling:
  #   "See <a href="/?action=index">the site map</a>."
  #
  # When not using this extension, authors must add this Wiki's base URL:
  #  "See [[http://www.oddmuse.com/cgi-bin/oddmuse?action=index|the site map]]."
  # my $UrlChars = '[-a-zA-Z0-9/@=+$_~*.,;:?!\'"()&#%]';    # see RFC 2396
  # $FullUrlPattern = "((?:$UrlProtocols:|/)$UrlChars+)";

  # Permit page authors to link to other pages having semicolons in
  # their names.

  # my $LinkCharsSansZero = "-;,.()' _1-9A-Za-z\x{0080}-\x{fffd}";
  # my $LinkChars = $LinkCharsSansZero.'0';
  # $FreeLinkPattern = "([$LinkCharsSansZero]|[$LinkChars][$LinkChars]+)";
}

# ....................{ MARKUP                             }....................
push(@MyRules,
     \&OrgHeadingRule,
     \&OrgTablesRule,
     \&OrgGreaterElementsRule,
     \&OrgListRule,
     \&OrgElementsRule,
     \&OrgObjectsRule
    );

# Org heading rules must come after the TocRule.
$RuleOrder{\&OrgHeadingRule} = 100;
$RuleOrder{\&OrgTablesRule} = 110;
$RuleOrder{\&OrgGreaterElementsRule} = 120;
$RuleOrder{\&OrgListRule} = 130;
$RuleOrder{\&OrgElementsRule} = 140;

$RuleOrder{\&OrgObjectsRule} = 150;;

# Oddmuse's built-in ListRule conflicts with above OrgListRule.  Thus,
# we ensure the latter is applied before the former.
$RuleOrder{\&ListRule} = 190;
# Oddmuse LinkRules must come after OrgTextMarkupRule.
$RuleOrder{\&LinkRules} = 200;
$RuleOrder{\&LineBreakRule} = 210;

=head2 OrgGreaterElementsRule

=cut
sub OrgGreaterElementsRule {
  # :DRAWER_NAME:
  #   Contents
  # :END:
  if (m/\G(\n+|^)([ \t]*:[^\n]+:[ \t]*\n.+?\n[ \t]*:END:[ \t]*(?=(\n|$)))/cgsi) {
    return '';
  }

  # #+BEGIN_QUOTE
  #  Contents
  # #+END_QUOTE
  if (m/\G(\n+|^)[ \t]*#\+begin_quote[ \t]*?(?=(\n|$))/cgi) {
    return CloseHtmlEnvironments()
      .AddHtmlEnvironment('blockquote').AddHtmlEnvironment('p');
  }

  if (InElement('blockquote') &&
      m/\G\n[ \t]*#\+end_quote[ \t]*?(?=(\n|$))/cgi) {
    return CloseHtmlEnvironment('blockquote').AddHtmlEnvironment('p');
  }

  # A "((...))" footnote anywhere in a page.
  #
  # Footnotes and the set of all footnotes must be marked so as to ensure their
  # reevaluation, as each of the footnotes might contain Wiki markup requiring
  # reevaluation (like, say, free links).

  # Regular footnotes - [fn:LABEL] DEFINITION
  if (m/\G((\n+|^)\[(\d+?|fn:.+?)\][ \t]*((.+?)(?=(\n{3,}|\n+\[fn:.+?\]|\n+\*+[ \t]+|$))))/gcos) {
    Dirty($1);			# do not cache the prefixing "\G"
    my $footnote_label = $3;
    my $footnote_text = $4;
    my $footnote_number = FootnoteUpdateRecord($footnote_label, $footnote_text);
    return '';
  }

  return undef;
}

=head2 OrgObjectsRule

=cut
sub OrgObjectsRule {
  return
    OrgFootnotesReferenceRule(@_)
    // OrgLinksRule(@_)
    // OrgTextMarkupRule(@_)
    // OrgSubAndSuperscriptRule(@_)
    // OrgLineBreaksRule(@_)
    ;
}

=head2 OrgElementsRule

=cut
sub OrgElementsRule {
  # horizontal rule
  # Four or more dashes.

  # NOTE: Org insists on 5 or more dashes.  wiki.pl uses 4 dashes for
  # separating out comments in the comments page.  In order to get
  # good looking comment pages on the Wiki, relax the rule a bit here
  # and admit 4 dashes.
  if (m/\G(\n+|^)[ \t]*-{4,}[ \t]*(?=(\n|$))/cg) {
    return CloseHtmlEnvironments().$q->hr().AddHtmlEnvironment('p');
  }

  # Fixed-width blocks
  # : line-1
  # : line-2
  if (m/\G(\n+|^)((:(\n| [^\n]*))(\n(:(\n| [^\n]*)))*(?=(\n|$)))/cgsi) {
    my $str = $2;
    # Strip the leading colon and space from the matched string.
    $str =~ s/^: ?//msg;
    return CloseHtmlEnvironments()
      .$q->pre({-class=> 'real'}, $str)
      .AddHtmlEnvironment('p');
  }

  # #+BEGIN_SRC
  #   Source Block
  # #+END_SRC
  if (m/\G(\n+|^)([ \t]*)#\+begin_src[ \t]*([^\n]*)\n(.*?)\n\2#\+end_src[ \t]*(?=(\n|$))/cgsi) {
    my ($prefix, $lang, $lines) = ($2, $3, $4);
    # Strip leading whitespaces from the source lines.
    $lines =~ s/^$prefix,?//msg;
    # Do Syntax Highlighting
    if (defined(&HighlightSyntax)) {
      $lines = HighlightSyntax($lines, $OrgLanguageToKateLanguage{$lang});
    }
    return CloseHtmlEnvironments()
      .$q->pre({-class=> 'real'}, $lines)
      .AddHtmlEnvironment('p');
  }

  # #+BEGIN_EXAMPLE
  #   Example Block
  # #+END_EXAMPLE
  if (m/\G(\n+|^)([ \t]*)#\+begin_example[ \t]*[^\n]*\n(.*?)\n\2#\+end_example[ \t]*(?=(\n|$))/cgsi) {
    my ($prefix, $lines) = ($2, $3);
    # Strip leading whitespaces and the quoting comma from the example
    # lines.
    $lines =~ s/^$prefix,?//msg;
    return CloseHtmlEnvironments()
      .$q->pre({-class=> 'real'}, $lines)
      .AddHtmlEnvironment('p');
  }

  # Comment and Meta lines - strip those
  if (m/\G(\n+|^)([ \t]*#\+?[^\n]*)(\n([ \t]*#\+?[^\n]*))*(?=(\n|$))/cgsi) {
    return '';
  }

  # 1 or more newlines followed by (optional) spaces
  if (m/\G(\n+|^)([ ]*)/cg) {
    my $num_newlines = length($1);
    my $indentation = length($2);
    my ($html, $open_new) = MoveUpOrDownList($num_newlines, $indentation);

    if ($open_new) {
      $html .= AddHtmlEnvironment('p');
    }

    return $html if $html;

    # if we are in a continuation line we might not have added any
    # markup.  Emit a space to account for the newline that we
    # matched.  Return an empty string so that rule matching
    # progresses.
    Clean(' ');
    return '';
  }

  return undef;
}

=head2 OrgTablesRule

=cut
sub OrgTablesRule {
  # Table syntax is matched last (or nearly last), so as to allow
  # other Org- specific syntax within tables.
  #
  # tables using | -- end of the table (two newlines) or row (one
  # newline)
  if (InElement('table')) {
    # We know that this is the end of this table row, if we match: *
    # an explicit "|" character followed by: a newline character and
    # another "|" character; or * an explicit newline character
    # followed by: a "|" character.
    #
    # That is to say, the "|" character terminating a table row is
    # optional.
    #
    # In either case, the newline character signifies the end of this
    # table row and the "|" character that follows it signifies the
    # start of a new row. We avoid consuming the "|" character by
    # matching it with a lookahead.
    if (m/\G([ \t]*\|)?[ \t]*\n(?=$OrgTableCellPattern)/cg) {
      # Case 1: Horizontal ruler that is followed with a regular table
      # cell.  Start a rowgroup.
      if (m/\G$OrgTableHrulePattern\n(?=$OrgTableCellPattern)/cg) {
	return CloseHtmlEnvironmentUntil('table').AddHtmlEnvironment('tbody')
	    .AddHtmlEnvironment('tr');
      }
      # Case 2: Horizontal ruler that terminates the table.  Close the
      # table.
      elsif (m/\G$OrgTableHrulePattern(\n|$)/cg) {
	return CloseHtmlEnvironment('table').AddHtmlEnvironment('p');
      }
      # Case 3: Regular table row.
      else {
	return CloseHtmlEnvironmentUntil('tbody').AddHtmlEnvironment('tr');
      }
    }
    # If block level elements are allowed in table cells, we know that
    # this is the end of the table, if we match:

    #  * an explicit "|" character followed by: a newline character
    #    not followed by another "|" character, or an implicit
    #    end-of-page.
    #
    # Otherwise, we know that this is the end of the table, if we
    # match:
    #  * an explicit "|" character followed by: a newline character
    #    not followed by another "|" character, or an implicit
    #    end-of-page; or
    #  * two newline characters.
    #
    # This condition should appear after the end-of-row test, above.
   elsif (m/\G[ \t]*\|[ \t]*(?=(\n|$))/cg or
           (!$OrgTableCellsContainBlockLevelElements and m/\G[ \t]*\n\n/cg)) {
      # Note: we do not call "CloseHtmlEnvironmentsOrgOld", as that
      #       function refers to the Oddmuse built-in. If another
      #       module with name lexically following "org.pl" also
      #       redefines the built-in "CloseHtmlEnvironments" function,
      #       then calling the "CloseHtmlEnvironmentsOrgOld" function
      #       causes that other module's redefinition to not be
      #       called. (Yes; an entangling mess we've made for
      #       ourselves, here. Clearly, this needs a rethink in some
      #       later Oddmuse refactoring.)
      return CloseHtmlEnvironment('table').AddHtmlEnvironment('p');
    }
    # Lastly, we know this this is start of a new table cell (and
    # possibly also the end of the last table cell), if we match: * an
    # explicit "|" character.
    #
    # This condition should appear after the end-of-table test, above.
    elsif (m/\G$OrgTableCellPattern/cg) {
      # This is the start of a new table cell. However, we only
      # consider that equivalent to the "$bol_fixme" variable when the
      # "$OrgTableCellsContainBlockLevelElements" variable is
      # enabled. (In other words, we only declare that we may insert
      # block level elements at the start of this new table cell, when
      # we allow block level elements in table cells. Yum.)
      $OrgIsTableCellBol = $OrgTableCellsContainBlockLevelElements;
      my $tag = $2 ? 'th' : 'td';
      my $attributes;
      return
         (InElement('td') || InElement('th') ? CloseHtmlEnvironmentUntil('tr') : '')
        .AddHtmlEnvironment($tag, $attributes);
    }
  }
  # tables using | -- an ordinary table cell
  #
  # Please note that order is important, here; this should appear
  # after all markup dependent on being in a current table.
  #
  # Also, the "|" character also signifies the start of a new table
  # cell. Thus, we avoid consuming that character by matching it with
  # a lookahead.
  elsif (m/\G(\n+|^)(($OrgTableHrulePattern\n)?)(?=$OrgTableCellPattern)/cg) {
    return OpenHtmlEnvironment('table', 1, 'class="user"' . $OrgTableHtmlAttributes)
	.AddHtmlEnvironment('tbody').AddHtmlEnvironment('tr');
  }

  return undef;
}

=head2 OrgFootnotesReferenceRule

=cut
sub OrgFootnotesReferenceRule {
  # A "((...))" footnote anywhere in a page.
  #
  # Footnotes and the set of all footnotes must be marked so as to ensure their
  # reevaluation, as each of the footnotes might contain Wiki markup requiring
  # reevaluation (like, say, free links).

  # Anonymous footnotes - [fn::DEFINITION]
  if (m/\G(\[fn::(.+?)\])/gcos) {
    Dirty($1);			# do not cache the prefixing "\G"
    my $footnote_text = $2;
    my $footnote_number = FootnoteUpdateRecord(undef, $footnote_text);

    printf $FootnoteFormat,
      $q->a({-href=> '#footnotes'.$footnote_number,
	     -name=>  'footnote' .$footnote_number,
	     -title=> 'Footnote: '.	# Truncate link titles to one line.
	     (length($footnote_text) >  48
	      ? substr($footnote_text, 0, 44).'...'
	      :        $footnote_text),
	     -class=> 'footnote'
	    }, $footnote_number);

    return '';
  }
  # Inline footnotes - [fn:LABEL:DEFINITION]
  elsif (m/\G(\[(fn:[^]]+?):(.+?)\])/gcos) {
    Dirty($1);			# do not cache the prefixing "\G"
    my $footnote_label = $2;
    my $footnote_text = $3;

    my $footnote_number = FootnoteUpdateRecord($footnote_label, $footnote_text);

    printf $FootnoteFormat,
      $q->a({-href=> '#footnotes'.$footnote_number,
	     -name=>  'footnote' .$footnote_number,
	     -title=> 'Footnote: '. # Truncate link titles to one line.
	     (length($footnote_text) >  48
	      ? substr($footnote_text, 0, 44).'...'
	      :        $footnote_text),
	     -class=> 'footnote'
	    }, $footnote_number);

    return '';
  }
  # Footnote Reference - [fn:LABEL] or [NNN]
  elsif (m/\G(\[(\d+?|fn:.+?)\])(?=([ \t]*\[fn:(.+?)\])?)/gcos) {
    Dirty($1);			# do not cache the prefixing "\G"
    my $footnote_label = $2;
    my $is_adjacent_footnote = defined $3;
    my $footnote_number = FootnoteUpdateRecord($footnote_label);

    printf $FootnoteFormat,
      $q->a({-href=> '#footnotes' .$footnote_number,
	     -name=>  'footnote' .$footnote_number,
	     -title=> 'Footnote #'.$footnote_number,
	     -class=> 'footnote'
	    }, $footnote_number.($is_adjacent_footnote ? ', ' : ''));

    return '';
  }

  return undef;
}

=head2 OrgFootnotesSectionRule

=cut
sub OrgFootnotesSectionRule {
  # A "((...))" footnote anywhere in a page.
  #
  # Footnotes and the set of all footnotes must be marked so as to ensure their
  # reevaluation, as each of the footnotes might contain Wiki markup requiring
  # reevaluation (like, say, free links).

  # The "<footnotes>" list of all footnotes at the foot of a page.
  if ($bol && m/\G($FootnotesPattern)/gcios) {
    Clean(CloseHtmlEnvironments());
    Dirty($1);			# do not cache the prefixing "\G"

    if (@FootnoteLabels) {
      my ($oldpos, $old_) = (pos, $_);
      PrintFootnotes();
      Clean(AddHtmlEnvironment('p')); # if dirty block is looked at
				      # later, this will disappear
      ($_, pos) = ($old_, $oldpos); # restore \G (assignment order matters!)
    }

    return '';
  }

  return undef;
}

# ....................{ HTML OUTPUT                        }....................
*PrintFooterFootnotesOld = *PrintFooter;
*PrintFooter =             *PrintFooterFootnotes;

=head2 PrintFooterFootnotes

Appends the list of footnotes to the footer of the page, if and only
if the user-provided content for that page had no content matching
C<$FootersPattern>.  Thus, this function is an eleventh-hour fallback;
ideally, pages providing footnotes also provide an explicit place to
list those footnotes.

=cut
sub PrintFooterFootnotes {
  my @params = @_;
  if (@FootnoteLabels) {
    PrintFootnotes();
  }
  PrintFooterFootnotesOld(@params);
}

=head2 PrintFootnotes

Prints the list of footnotes.

=cut
sub PrintFootnotes() {
  print
    $q->start_div({-class=> 'footnotes'})
    .$q->h2(T($FootnotesHeaderText));

  # Don't use <ol>, because we want to link from the number back to
  # its page location.
  foreach my $footnote_label (@FootnoteLabels) {
    my $footnote_number = $FootnoteRecord{$footnote_label}[0];
    my $footnote_text = $FootnoteRecord{$footnote_label}[1];

    print
      $q->start_div({-class=> 'footnote'})
      .$q->a({-class=> 'footnote_backlink',
              -name=>  'footnotes'.$footnote_number,
              -href=> '#footnote' .$footnote_number}, $footnote_number.'.')
      .' ';
    print $q->start_div({-class=> 'footnote_content'});
    ApplyRules($footnote_text, 1);
    print $q->end_div();
    print $q->end_div();

  }

  print $q->end_div();

  # Empty the footnotes to disallow our calling the fallback, later.
  @FootnoteLabels = ();
  %FootnoteList = {};
  $FootnoteAnonymousCount = 0;
}

sub FootnoteUpdateRecord {
  my ($footnote_label, $footnote_text) = @_;

  # Assign an internally generated label for anonymous footnotes.
  $footnote_label = (':AnonymousLabel' . ++$FootnoteAnonymousCount)
    unless defined $footnote_label;

  if (not exists $FootnoteRecord{$footnote_label}) {
    # Footnote label is seen for the first time.  Assign it a number;
    push(@FootnoteLabels, $footnote_label);
    $footnote_number = @FootnoteLabels;
    $FootnoteRecord{$footnote_label} = [$footnote_number, $footnote_text];
  } else {
    # Footnote label already seen.  Get it's number;
    $footnote_number = $FootnoteRecord{$footnote_label}[0];
    # Note new definition if any.
    $FootnoteRecord{$footnote_label}[1] = $footnote_text
      if defined $footnote_text;
  }

  return $footnote_number;
}

=head2 OrgLinksRule

=cut
sub OrgLinksRule {
  # Inline images
  # - Local image
  #   [[pic.png]]
  if (m/\G(\[\[($FreeLinkPattern\.$ImageExtensions)\]\])/cgos) {
    my $text = $2;
    return GetOrgLinkHtml($1, GetDownloadLink(FreeToNormal($2), 1, undef, $text), $text);
  }

  # Inline image
  # - Remote image:
  #   [[url.png]]
  if (m/\G\[\[($FullUrlPattern$\.$ImageExtensions)\]\]/cgos) {
    return GetOrgImageHtml(
      $q->a({-href=> UnquoteHtml($1),
             -class=> 'image outside'},
            $q->img({-src=> UnquoteHtml($1),
                     -alt=> UnquoteHtml(''),
                     -class=> 'url outside'})));
  }

  # Links where description is an image
  # - Local destination and local image
  #   [[link][pic.png]]
  if (m/\G(\[\[$FreeLinkPattern$OrgLinkPipePattern
              ($FreeLinkPattern\.$ImageExtensions)\]\])/cgosx) {
    my $text = $2;
    return GetOrgLinkHtml($1, GetOrgImageHtml(
      ScriptLink(UrlEncode(FreeToNormal($2)),
                 $q->img({-src=> GetDownloadLink(FreeToNormal($3), 2),
                          -alt=> UnquoteHtml($text),
                          -class=> 'upload'}), 'image')), $text);
  }

  # - Local destination and remote image
  #   [[link][url.png]]
  if (m/\G(\[\[$FreeLinkPattern$OrgLinkPipePattern
              ($FullUrlPattern\.$ImageExtensions)\]\])/cgosx) {
    my $text = $2;
    return GetOrgLinkHtml($1, GetOrgImageHtml(
      ScriptLink(UrlEncode(FreeToNormal($2)),
                 $q->img({-src=> UnquoteHtml($3),
                          -alt=> UnquoteHtml($text),
                          -class=> 'url outside'}), 'image')), $text);
  }

  # - Remote destination and local image
  #   [[url][pic.png]]
  if (m/\G(\[\[$FullUrlPattern$OrgLinkPipePattern
              ($FreeLinkPattern\.$ImageExtensions)\]\])/cgosx) {
    my $text = $2;
    return GetOrgLinkHtml($1, GetOrgImageHtml(
      $q->a({-href=> UnquoteHtml($2), -class=> 'image outside'},
            $q->img({-src=> GetDownloadLink(FreeToNormal($3), 2),
                     -alt=> UnquoteHtml($text),
                     -class=> 'upload'}))), $text);
  }

  # - Remote destination and remote image
  #   [[url][url.png]]
  if (m/\G\[\[$FullUrlPattern$OrgLinkPipePattern
             ($FullUrlPattern\.$ImageExtensions)\]\]/cgosx) {
    return GetOrgImageHtml(
      $q->a({-href=> UnquoteHtml($1), -class=> 'image outside'},
            $q->img({-src=> UnquoteHtml($2),
                     -alt=> UnquoteHtml(''),
                     -class=> 'url outside'})));
  }

  # link: [[url]] and [[url][text]]
  if (m/\G\[\[$FullUrlPattern$OrgLinkTextPattern\]\]/cgos) {
    # Permit embedding of Org syntax within link text. (Rather
    # complicated, but it does the job remarkably.)
    my $link_url  = $1;
    my $link_text = $3 ? OrgTextMarkupRuleRecursive($3, @_) : $link_url;

    # GetUrl() takes parameters resembling:
    # ~ the link's URL.
    # ~ the link's text (to be displayed for that URL).
    # ~ a boolean (to be used Gods' know how).
    return GetUrl($link_url, $link_text, 1);
  }

  # link: [[page]] and [[page][text]]
  if (m/\G(\[\[$FreeLinkPattern$OrgLinkTextPattern\]\])/cgos) {
    my $markup =    $1;
    my $page_name = $2;
    my $link_text = $4 ? OrgTextMarkupRuleRecursive($4, @_) : $page_name;

    return GetOrgLinkHtml($markup,
      GetPageOrEditLink($page_name, $link_text, 0, 1), $link_text);
  }

  # interlink: [[Wiki:page]] and [[Wiki:page][text]]
  if ($is_interlinking and
         m/\G(\[\[$FreeInterLinkPattern$OrgLinkTextPattern\]\])/cgos) {
    my $markup =    $1;
    my $interlink = $2;
    my $interlink_text = $4;
    my ($site_name, $page_name) = $interlink =~ m~^($InterSitePattern):(.*)$~;

    # Permit embedding of Org syntax within interlink text. We operate
    # on "$interlink_text", rather than "$4", since that ordinal has
    # already been overridden by the above regular expression match.
    $interlink_text =       $interlink_text
      ? OrgTextMarkupRuleRecursive($interlink_text, @_)
      :  $q->span({-class=> 'site'}, $site_name)
        .$q->span({-class=> 'separator'}, ':')
        .$q->span({-class=> 'page'}, $page_name);

    # If the Wiki for this interlink is a registered Wiki (that is, it
    # appears in this Wiki's "$InterMap" page), then produce an
    # interlink to it; otherwise, produce a normal intralink to a page
    # on this Wiki.
    return GetOrgLinkHtml($markup,
        $InterSite{$site_name}
      ? GetInterLink     ($interlink, $interlink_text, 0, 1)
      : GetPageOrEditLink($page_name, $interlink_text, 0, 1), $interlink_text);
  }

  return undef;
}

=head2 OrgTextMarkupRule

Handles the large part of Wiki Org syntax.

Technically, as Oddmuse's default C<LinkRules> function also conflicts
with this extension's link rules and does not comply, in any case,
with the Wiki Org rules for links, we should also nullify Oddmuse's
default C<LinkRules> function. Sadly, we don't. Why? Since existing
Oddmuse Wikis using this extension depend on Oddmuse's default
C<LinkRules> function, and as it's no terrible harm to let that
function be, we have to let it be. Bah!

=cut
sub OrgTextMarkupRule {
  # "$is_interlinking" is a boolean that, if true, indicates this rule
  # should make interlinks (i.e., links to Wiki pages on other,
  # external Wikis) and, and, if false, should not. (Typically,
  # Oddmuse sets this to false when including external HTML pages into
  # local Wiki pages.)
  my ($is_interlinking, $is_intraanchoring) = @_;

  if (m!\G(\*\*|//|__|\+\+|~~|==)!cgos) {
    my $marker = $1;
    my $html_tag = $OrgEmphasisAlist{$marker};

    return AddOrCloseHtmlEnvironment($html_tag);
  }

  # Text markup - Bold, Italic, Underline, Verbatim, Monospace and
  # Strikethrough.

  if (m!\G([*/_+~=])!cgos) {
    my $pre = $-[0] ? substr($_, $-[0]-1, 1) : '';
    my $post = substr($_, $+[0], 1);
    my $marker = $1;
    my $html_tag = $OrgEmphasisAlist{$marker};

    if (InElement($html_tag)) {
      # End marker should be bound by non-whitespace on the left and
      # whitespace or punctuation character on the right.
      return CloseHtmlEnvironment($html_tag)
	if ($pre and $pre !~ /[\ \t\r\n,'\"]/ and
	    (not $post or $post =~ /[-\ \t.,:?;'\")\n*\/_+=~]/));
    } else {
      return AddHtmlEnvironment($html_tag)
	# Start marker should be bound by whitespace or punctuation
	# character on the left and non-whitespace character on the
	# right.
	if ((not $pre or $pre =~ /[\ \t('\"{\n*\/_+=~]/)
	    and $post !~ /[\ \t\r\n,'\"]/) ;
    }

    return $marker;
  }

  return undef;
}

=head2 OrgSubAndSuperscriptRule

=cut
sub OrgSubAndSuperscriptRule {
  # Superscript: ^^sup^^
  if ($OrgAdditionSupSub and m/\G\^\^/cg) {
    return AddOrCloseHtmlEnvironment('sup');
  }

  # Subscript: ,,sub,,
  if ($OrgAdditionSupSub and m/\G\,\,/cg) {
    return AddOrCloseHtmlEnvironment('sub');
  }

  return undef;
}

=head2 OrgLineBreaksRule

=cut
sub OrgLineBreaksRule {

  # Trailing whitespace
  if (m/\G[ \t]*(?=(\n|$))/cg) {
    return $OrgLineBreaks ? $q->br() : '';
  }

  # Explicit linebreak
  if (m/\G\\\\[ \t]*(?=(\n|$))/cg) {
    return $q->br();
  }

  return undef;
}

sub OrgHeadingRule {
  # header opening: = to ====== for h1 to h6
  #
  # header opening and closing have been partitioned into two separate
  # conditional matches rather than congealed into one conditional
  # match. Why?  Because, in so doing, we permit application of other
  # markup rules, elsewhere, to header text. This, in turn, permits
  # insertion and interpretation of complex markup in header text;
  # e.g., ** /This Is a *Level-2* Header Having Complex Markup./
  if (m~\G(^|\n)+(\*+)[ \t]+~cg) {
    my $header_depth = length($2);
    ($OrgHeaderHtmlTag, $OrgHeaderHtmlTagAttr) = $header_depth <= 6
      ? ('h'.$header_depth, '')
      : ('h6', qq{class="h$header_depth"});
    return CloseHtmlEnvironments()
      . AddHtmlEnvironment($OrgHeaderHtmlTag, $OrgHeaderHtmlTagAttr);
  }
  # header closing: = to ======, newline, or EOF
  #
  # Note: partitioning this from the heading opening conditional,
  # above, typically causes Oddmuse to insert an extraneous space at
  # the end of header tags. This is non-dangerous, fortunately; and
  # changes nothing.
  elsif ($OrgHeaderHtmlTag and m~\G[ \t]*(?=(\n|$))~cg) {
    my $header_html =
      CloseHtmlEnvironment($OrgHeaderHtmlTag, '^'.$OrgHeaderHtmlTagAttr.'$')
       .AddHtmlEnvironment('p');
    $OrgHeaderHtmlTag = $OrgHeaderHtmlTagAttr = '';
    return $header_html;
  }

  return undef;
}

sub GetParentIndentation {
    my $from = shift;
    my $i = $from // -1;
    my $maxIndex = $#HtmlStack;

    # Search searching the stack from an index one higher than the
    # passed in value or from zero.
    while (++$i <= $maxIndex) {
	my $current_indentation =
	    GetStashedAttributeValue($HtmlAttrStack[$i], '_indentation_');

	# Return a list of index and current indentation.
	return ($i, $current_indentation)
	    if (defined ($current_indentation));
    }

    # No parent indentation is found.
    return (-1, -1);
}

sub GetSiblingOrParent {
    my $indentation = shift;
    my $maxIndex = $#HtmlStack;
    my $i = -1;
    my $current_indentation = -1;

    while ($i <= $maxIndex) {
	($i, $current_indentation) = GetParentIndentation($i);
	# No more parents.
	last if ($i eq -1);

	# Found a level that is at the same level as or is the parent
	# of the requested indentation.  Return it.
	return ($i, $current_indentation)
	    if ($current_indentation <= $indentation);
    };

    # No parent level exists.
    return (-1, -1);
}

sub SwitchToIndentation {
    my ($indentation) = @_;
    my $html = '';

    # If we are switching to level-0, there is nothing much to do.
    # Close all open environments and return.
    return CloseHtmlEnvironments()
	unless $indentation;

    # Get sibling or parent.
    my ($i, $current_indentation) = GetSiblingOrParent($indentation);

    # None found i.e., Leaving the current list context.
    if ($i eq -1) {
	# Close everything and return.
	$html .= CloseHtmlEnvironments();
	return $html;
    }

    # Found a Sibling or a Parent.

    # Set index to the list item i.e., Leave the 'li' tag on the stack
    # at the exit of this subroutine.
    --$i;

    # Save the parent context.
    my @stack = splice(@HtmlStack, $i);
    my @attrstack = splice(@HtmlAttrStack, $i);

    # Close everything contained within 'li'.
    my $html = CloseHtmlEnvironments();

    # Restore the stack context.
    @HtmlStack = @stack;
    @HtmlAttrStack = @attrstack;

    return $html;
}

sub OpenHtmlEnvironmentUseIndentation {
    my ($html_tag, $indentation, $html_tag_attr) = @_;

    my $html = SwitchToIndentation($indentation);
    my $current_indentation = GetParentIndentation();

    if ($current_indentation != -1 and
	$current_indentation eq $indentation) {
	# Sibling context.  Close the list item.  This leave 'ol' or
	# 'ul' on the stack.
	return $html .= CloseHtmlTag(shift(@HtmlStack), shift(@HtmlAttrStack));
    }

    # FIXME:
    # $depth = $IndentLimit if $depth > $IndentLimit; # requested depth
    # 						    # 0 makes no sense

    # A new top-level list or a sublist.  Open a 'ul' or 'ul'.

    # backwards-compatibility hack: classically, the third argument to
    # this function was a single CSS class, rather than string of HTML
    # tag attributes as in the second argument to the
    # "AddHtmlEnvironment" function. To allow both sorts, we
    # conditionally change this string to 'class="$html_tag_attr"'
    # when this string is a single CSS class.
    $html_tag_attr = qq/class="$html_tag_attr"/
	if $html_tag_attr && $html_tag_attr !~ m/^\s*.+?\s*=\s*('|").+\1/;

    # Stash the current indentation level within the tag
    $html_tag_attr =
	StashAttributeValue($html_tag_attr, '_indentation_', $indentation);

    # Open the new tag.
    $html .= AddHtmlEnvironment($html_tag, $html_tag_attr);

    return $html;
}

=head2 OrgListRule

=cut
sub OrgListRule {
    my $html = '';

    # 1 or more newlines followed by (optional) spaces and (optional)
    # list bullet.
    if (m/\G(\n+|^)([ ]*)((\d+.|[-+*]) )/cg) {
	my $num_newlines = length($1);
	my $indentation = length($2);
	my $bullet = $4;

	if ($num_newlines > 2) {
	    # Three or more newlines
	    $html .= CloseHtmlEnvironments();
	}

	# Found a list item.

	# List items are considered to be indented to a level one
	# greater than the leading whitespace.  Thus, they are
	# always indented by atleast 1.
	++$indentation;

	# Ordered or Un-ordered?
	my $html_tag = ($bullet =~ /[-+*]/) ? 'ul' : 'ol';

	$html .=
	    OpenHtmlEnvironmentUseIndentation($html_tag, $indentation)
	    .AddHtmlEnvironment ('li');

	return $html;
    }

    return undef;
}

sub MoveUpOrDownList {
    my ($num_newlines, $indentation) = @_;
    my $html = '';

    # 1 or more newlines followed by (optional) spaces.
    if ($num_newlines > 2) {
	# Three or more newlines
	$html .= CloseHtmlEnvironments();
    }

    # Found an indented line.  May or may not be a paragraph.

    # Regular lines are considered to be indented to the same level as
    # the leading whitespace.  Thus, their indentation is greater than
    # or equal to zero.
    my $parent_indentation = GetParentIndentation();
    my $current_indentation =  ($parent_indentation == -1) ? 0
	: $parent_indentation;

    # Force lines that are more indented than the current level to
    # be at the same level as the current level.
    if ($indentation > $current_indentation) {
	$indentation = $current_indentation;
    }

    my $open_new = !$current_indentation # outside of a list
	|| ($indentation < $current_indentation) # within a list, but leaving it
	|| ($num_newlines > 1)			 # staying within the
						 # list, but two new
						 # lines seen.
	;

    if ($indentation < $current_indentation or $num_newlines > 1) {
	$html .= SwitchToIndentation($indentation);
    }

    return ($html, $open_new);
}

# ....................{ HTML                               }....................

=head2 GetOrgImageHtml

Returns the passed HTML image, conditionally wrapped within an HTML
paragraph tag having an necessary image class when the passed HTML
also represents such a new paragraph. Difficult to explain, isn't she?

=cut
sub GetOrgImageHtml {
  my $image_html = shift;
  my $bol_fixme = (m/\G(^|(?<=\n))/cg);

  return
      ($bol_fixme ? CloseHtmlEnvironments().AddHtmlEnvironment('p', 'class="image"') : '')
    .$image_html;
}

=head2 GetOrgLinkHtml

Marks the passed HTML as a dirty block, unless this HTML belongs to an
HTML header tag. Such tags may not contain dirty blocks! Most Oddmuse
modules using header tags (e.g., "sidebar.pl", "toc.pl") require, as a
caching efficiency, header text to be clean. This is a nearly
necessary efficiency, since regeneration of markup for those modules
is an often costly operation. (We certainly don't want to regenerate
the Table of Contents for each page having at least one header having
at least one dirty link whenever an external user browses to that
page!)

Thus, if in a header, this function cleans links out of the passed
HTML and returns the resultant HTML (to the current clean
block). Otherwise, this function appends the resultant HTML to a new
dirty block, prints it, and returns it. (This does not print the
resultant HTML when clean, since clean blocks are printed,
automatically, by the next call to C<Dirty>.)

This function, lastly, accepts three function parameters. These are:

=over

=item C<$markup>. (This is the Wiki markup string to be marked as
      dirty when it is not embedded in a Org header.)

=item C<$html>. (This is the HTML string to be marked as dirty when
      this HTML is not embedded in a Org header.)

=item C<$text>. (This is the text string to be marked as clean when
      this HTML is embedded within a Org header.)

=back

Org functions, above, should **not** call the C<Dirty> function
directly.  Rather, they should always call this function...with
appropriate parameters.

=cut
sub GetOrgLinkHtml {
  my ($markup, $html, $link_text) = @_;

  if ($OrgHeaderHtmlTag) { return $link_text; }
  else {
    Dirty($markup);
    print $html;
    return '';
  }
}

# ....................{ FUNCTIONS }....................
*RunMyRulesOrgOld = *RunMyRules;
*RunMyRules =          *RunMyRulesOrg;

=head2 RunMyRulesOrg

Runs all markup rules for the current block of page markup. This
redefinition ensures that the beginning of a table cell is considered
the beginning of a block-level element -- that, in other words, the
C<$bol_fixme> global be set to 1.

If the C<$OrgTableCellsContainBlockLevelElements> option is set to 0
(the default), then this function is, effectively, a no-op - and just
calls the default C<RunMyRules> function.

=cut
sub RunMyRulesOrg {
  # See documentation for the "$OrgIsTableCellBol" variable, above.
  my $org_is_table_cell_bol_last = $OrgIsTableCellBol;
  $bol_fixme = 1 if $OrgIsTableCellBol;
  my $html = RunMyRulesOrgOld(@_);
  $OrgIsTableCellBol = '' if $org_is_table_cell_bol_last;

  return $html;
}

=head2 OrgTextMarkupRuleRecursive

Calls C<OrgTextMarkupRule> on the passed string, from within some
existing call to C<OrgTextMarkupRule>. This function ensures, among
other safeties, that the C<OrgTextMarkupRule> function is not recursed
into more than once.

=cut
sub OrgTextMarkupRuleRecursive {
  my     $markup = shift;
  return $markup if $OrgTextMarkupRuleRecursing;  # avoid infinite loops
              local $OrgTextMarkupRuleRecursing = 1;
              local $bol_fixme = 0;  # prevent block level element
				     # handling

  # Preserve global variables.
  my ($oldpos, $old_) = (pos, $_);
  my @oldHtmlStack =     @HtmlStack;
  my @oldHtmlAttrStack = @HtmlAttrStack;

  # Reset global variables.
  $_ = $markup;
  @HtmlStack = @HtmlAttrStack = ();

  my ($html, $html_org) = ('', '');

  # The contents of this loop are, in part, hacked from the guts of
  # Oddmuse's ApplyRules() function. We cannot simply call that
  # function, as it "cleans" the HTML converted from the text passed
  # to it, rather than returns that HTML.
  while (1) {
    if ($html_org = OrgTextMarkupRule(@_) or
       ($OrgIsOrgAddition and  # try "orgaddition.pl", too.
        $html_org = OrgAdditionRule(@_))) {
      $html .= $html_org;
    }
    elsif (m/\G&amp;([a-z]+|#[0-9]+|#x[a-fA-F0-9]+);/cg) { # entity references
      $html .= "&$1;";
    }
    elsif (m/\G\s+/cg) {
      $html .= ' ';
    }
    elsif (   m/\G([A-Za-z\x{0080}-\x{fffd}]+([ \t]+[a-z\x{0080}-\x{fffd}]+)*[ \t]+)/cg
           or m/\G([A-Za-z\x{0080}-\x{fffd}]+)/cg
           or m/\G(\S)/cg) {
      $html .= $1;  # multiple words but do not match http://foo
    }
    else { last; }
  }

  # Restore global variables, in reverse order.
  @HtmlAttrStack = @oldHtmlAttrStack;
  @HtmlStack =     @oldHtmlStack;
  ($_, pos) = ($old_, $oldpos);

  # Allow entrance into this function, again.
  $OrgTextMarkupRuleRecursing = 0;

  return $html;
}

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2004, 2006-7  Alex Schroeder <alex@gnu.org>;
Copyleft  (C) 2008 Brian Curry <http://raiazome.com>;
Copyright (C) 2008 Weakish Jiang <weakish@gmail.com>;
Copyright (C) 2013 Jambunathan K <kjambunathan@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see L<http://www.gnu.org/licenses/>.

=cut
