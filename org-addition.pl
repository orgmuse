#!/usr/bin/env perl
# ====================[ orgaddition.pl                  ]====================

=head1 NAME

orgaddition - An Oddmuse module for augmenting the Org Markup module with
                 so-called, unofficial "Org Additions" markup.

=head1 INSTALLATION

orgaddition is simply installable; simply:

=over

=item First install the Org Markup module; see
      L<http://oddmuse.org/wiki/Org_Markup_Extension>.

=item Move this file into the B<wiki/modules/> directory for your Oddmuse Wiki.

=back

=cut
package OddMuse;

$ModulesDescription .= '<p>See <a href="http://oddmuse.org/wiki/Org_Markup_Extension">Org Markup Extension</a></p>';

# ....................{ CONFIGURATION                      }....................

=head1 CONFIGURATION

orgaddition is easily configurable; set these variables in the
B<wiki/config.pl> file for your Oddmuse Wiki.

=cut
# Since these rules are not official now, users can turn off some of
# them.
use vars qw($OrgAdditionSupSub
            $OrgAdditionDefList
            $OrgAdditionInlineQuote
          );

=head2 $OrgAdditionSupSub

A boolean that, if true, enables this extension's handling of
^^supscript^^ and ,,subscript,,-style markup. (By default, this boolean is
true.)

=cut
$OrgAdditionSupSub = 0;

=head2 $OrgAdditionDefList

A boolean that, if true, enables this extension's handling of
"; definition : lists"-style markup. (By default, this boolean is true.)

=cut
$OrgAdditionDefList = 0;

=head2 $OrgAdditionInlineQuote

A boolean that, if true, enables this extension's handling of ''inline
quote''-style markup. (By default, this boolean is false.)

=cut
$OrgAdditionInlineQuote = 0;

# ....................{ MARKUP                             }....................
push(@MyRules, \&OrgAdditionRule);
SetHtmlEnvironmentContainer('blockquote');

# Blockquote line-breaks conflict with Org-style line-breaks.
$RuleOrder{\&OrgAdditionRule} = -11;

sub OrgAdditionRule {
  # definition list
  # ; dt
  # : dd
  if ($OrgAdditionDefList && $bol && m/\G\s*\;[ \t]*(?=(.+(\n)(\s)*\:))/cg
      or InElement('dd') && m/\G\s*\n(\s)*\;[ \t]*(?=(.+\n(\s)*\:))/cg) {
    return CloseHtmlEnvironmentUntil('dd') . OpenHtmlEnvironment('dl', 1)
      . AddHtmlEnvironment('dt');
  }				# `:' needs special treatment, later
  elsif (InElement('dt') and m/\G\s*\n(\s)*\:[ \t]*(?=(.+(\n)(\s)*\:)*)/cg) {
    return CloseHtmlEnvironment() . AddHtmlEnvironment('dd');
  } elsif (InElement('dd') and m/\G\s*\n(\s)*\:[ \t]*(?=(.+(\n)(\s)*\:)*)/cg) {
    return  CloseHtmlEnvironment() . AddHtmlEnvironment('dd');
  }
  # ''inline quotes''
  elsif ($OrgAdditionInlineQuote and m/\G\'\'/cgs) {
    return AddOrCloseHtmlEnvironment('q');
  }

  return undef;
}

=head1 COPYRIGHT AND LICENSE

The information below applies to everything in this distribution,
except where noted.

Copyleft  2008 by B.w.Curry <http://www.raiazome.com>.
Copyright 2008 by Weakish Jiang <weakish@gmail.com>.
Copyright 2009  Alex Schroeder <alex@gnu.com>
Copyright 2013  Jambunathan K <kjambunathan@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see L<http://www.gnu.org/licenses/>.

=cut
