# Copyright (C) 2007, 2008  Alex Schroeder <alex@emacswiki.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
#    Free Software Foundation, Inc.
#    59 Temple Place, Suite 330
#    Boston, MA 02111-1307 USA

require 't/test.pl';
package OddMuse;
use Test::More tests => 39;

clear_pages();

add_module('org.pl');

run_tests(split('\n',<<'EOT'));
@1 a mix of =text markups=: *bold*, /italic/, _underline_, +strike through+ and ~verbatim~.
<p>@1 a mix of <code>text markups</code>: <strong>bold</strong>, <em>italic</em>, <u>underline</u>, <del>strike through</del> and <tt>verbatim</tt>.</p>
@3 one\ntwo
<p>@3 one two</p>
@4 one\n\ntwo\n\nthree
<p>@4 one</p><p>two</p><p>three</p>
1. @5 one\n2. two\n3. three
<ol><li>@5 one</li><li>two</li><li>three</li></ol>
- @6 one\n- two\n- three
<ul><li>@6 one</li><li>two</li><li>three</li></ul>
+ @7 one\n+ two\n+ three
<ul><li>@7 one</li><li>two</li><li>three</li></ul>
@8 Some text\n\n1. one\n\n2. two\n\n3. three
<p>@8 Some text</p><ol><li>one</li><li>two</li><li>three</li></ol>
* @9 heading1\n** heading2\n*** heading3\n**** heading4
<h1>@9 heading1</h1><h2>heading2</h2><h3>heading3</h3><h4>heading4</h4>
 * @10 one\n * two\n * three
<ul><li>@10 one</li><li>two</li><li>three</li></ul>
@11 one one\none one\n\ntwo two\ntwo two
<p>@11 one one one one</p><p>two two two two</p>
1. @12 N-1.1\n2. N-1.2\n3. N-1.3\n   1. N-1.3.1\n   2. N-1.3.2\n      1. N-1.3.2.1\n         1. N-1.3.2.1.1\n   3. N-1.3.3\n   4. N-1.3.4\n      1. N-1.3.4.1\n      2. N-1.3.4.2\n4. N-1.4\n   1. N-1.4.1\n   2. N-1.4.2\n      1. N-1.4.2.1\n      2. N-1.4.2.2
<ol><li>@12 N-1.1</li><li>N-1.2</li><li>N-1.3<ol><li>N-1.3.1</li><li>N-1.3.2<ol><li>N-1.3.2.1<ol><li>N-1.3.2.1.1</li></ol></li></ol></li><li>N-1.3.3</li><li>N-1.3.4<ol><li>N-1.3.4.1</li><li>N-1.3.4.2</li></ol></li></ol></li><li>N-1.4<ol><li>N-1.4.1</li><li>N-1.4.2<ol><li>N-1.4.2.1</li><li>N-1.4.2.2</li></ol></li></ol></li></ol>
@13 Some text\n\n1. one\n\n2. two\n\n3. three\n\nSome more text\n\n- one\n\n- two\n\n- three\n\nEven more text
<p>@13 Some text</p><ol><li>one</li><li>two</li><li>three</li></ol><p>Some more text</p><ul><li>one</li><li>two</li><li>three</li></ul><p>Even more text</p>
1. @14 N-1\n   - B-1.1\n     continuation line\n     continuation line\n\n     First paragraph\n     continuation line\n\n     Second Paragraph\n     continuation line
<ol><li>@14 N-1<ul><li>B-1.1 continuation line continuation line<p>First paragraph continuation line</p><p>Second Paragraph continuation line</p></li></ul></li></ol>
EOT
